let express = require('express')
let app = express()
//importer la connection
let mysql = require('mysql')

/***************************DATABASE MYSQL***************************/
//create connection
const db = mysql.createConnection({
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'nodemysql'
})
//Connect
db.connect((err) => {
    //if(err) throw error
    console.log('MySql connected!')
})
/********************************************************************/

////////////////////////////////////Les moteurs de template////////////////////////
app.set('view engine', 'ejs')
/////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////Les USES//////////////////////////////////////
app.use('/assets', express.static('public'))
///////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////Les redirections///////////////////////////////
//lancement de la page racine
app.get('/', (request, response) => {

    response.render('index')
})

//redirection vers la page about
app.get('/about', (request, response) => {

    response.render('about')
})

//redirection vers la page services
app.get('/services', (request, response)=>{

    response.render('services')
})

//redirection vers la page contact
app.get('/contact', (request, response)=>{

    response.render('contact')
})

//creation de la base de donnees
app.get('/createdb', (request, response) => {
    let SQL = 'CREATE DATABASE nodemysql'
    db.query(SQL, (err, result) => {
        console.log(result)
        response.send('Database created')
    })
})

//creation des tables
app.get('/createposttables', (request, response) => {
    let SQL = 'CREATE TABLE posts(id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY (id))'
    db.query(SQL, (err, result) => {
        console.log(result)
        response.send('POSTS TABLE CREATED')
    })
})


//insertion dans la table posts
app.get('/addposts', (request, response) => {
    let post = {title : 'First post', body : 'This is post number 1'}
    let SQL = 'INSERT INTO posts SET ?'
    let query= db.query(SQL, post, (result) => {
        console.log(result)
        response.send('Post 1 added')
    })
})


//selection des donnees dans la table posts
app.get('/getposts', (request, response) => {
    let SQL = 'SELECT * FROM posts'
    let query= db.query(SQL, (err, results) => {
        if(err) throw err
        //console.log(results)
        response.render('select', {lesposts : results})
    })
})


app.get('/deutsche', (request, response, next) => {
    let SQL = "SELECT * FROM langages WHERE id_lng='de'"
    let query = db.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('about', {langues : results})
    })
})

app.get('/english', (request, response, next) => {
    let SQL = "SELECT * FROM langages WHERE id_lng='eng'"
    let query = db.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('about', {langues : results})
    })
})

app.get('/french', (request, response, next) => {
    let SQL = "SELECT * FROM langages WHERE id_lng='fr'"
    let query = db.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('about', {langues : results})
    })
})

app.get('/turc', (request, response, next) => {
    let SQL = "SELECT * FROM langages WHERE id_lng='tr'"
    let query = db.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('about', {langues : results})
    })
})

//////////////////////////////////////////////////////////////////////////////////


//ecoute le port
app.listen(3000, () => {
    console.log('Server started on port 3000')
})